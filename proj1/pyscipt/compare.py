import os

evaluations = "../evaluations"

evals = map(lambda x: os.path.join(evaluations, x), os.listdir(evaluations))

def takeAP(fname):
	f = open(fname)
	d = f.readlines()[-1]
	f.close()

	ap = d.split()[4]
	name = fname.split("/")[2].split(".")[1]
	return (name, ap) 

def takeNDCG(fname):
	f = open(fname)
	d = f.readlines()[-1]
	f.close()

	ap = d.split()[-1]
	name = fname.split("/")[2].split(".")[1]
	return (name, ap) 

APs = [takeAP(f) for f in evals]
NDCGs = [takeNDCG(f) for f in evals]

rankedAP = sorted(APs, key=lambda x: x[1], reverse=True)
rankedNDCG = sorted(NDCGs, key = lambda x: x[1], reverse=True)
print len(rankedAP)
print map(lambda x: x[0], rankedAP).index('proj1')
print map(lambda x: x[0], rankedNDCG).index('proj1')
