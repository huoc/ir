// set the name of the project
name := "proj1"

version := "0.1"

organization := "University of Delaware"

scalaVersion := "2.10.0"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

libraryDependencies ++= Seq(
	"com.typesafe" % "config" % "0.5.+",
	"org.apache.lucene" % "lucene-core" % "4.10.0",
	"org.apache.lucene" % "lucene-queryparser" % "4.10.0",
	"org.jsoup" % "jsoup" % "1.7.2",
	"com.github.rholder" % "snowball-stemmer" % "1.3.0.581.1",
	"org.apache.lucene" % "lucene-analyzers-common" % "4.10.0")
