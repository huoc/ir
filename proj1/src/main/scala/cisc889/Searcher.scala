package cisc889

import java.io.File

import scala.Array.canBuildFrom

import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexReader
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.util.Version

case class Searcher(val formatted : String)

object Searcher {
	def apply(src: File, queries: Iterable[Query]): Searcher = {
		val dir = FSDirectory open src
		implicit val index = IndexReader open dir
		implicit val searcher = new IndexSearcher(index)

		val analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT)
		implicit val qp = new QueryParser(Version.LUCENE_CURRENT, "text", analyzer)
		
		val formatted = queries map {topDocs(_)} mkString "\n"
		index.close()
		new Searcher(formatted)
	}
	
	def topDocs(query: Query)(implicit qp: QueryParser, searcher: IndexSearcher, index: DirectoryReader): String = {
		val q = qp parse (query.q)
		val results = searcher search (q, index.numDocs)
		
//		println(results.totalHits)

		results.scoreDocs take 1000 map { scoreDoc =>
			{
				val docno = index document scoreDoc.doc getField "DOCNO" stringValue
				val rank = (results.scoreDocs indexOf scoreDoc) + 1
				val score = scoreDoc.score
				List(query.id, "XX", docno, rank.toString, score.toString, "lucene") mkString "\t"
			}
		} mkString "\n"
	}
}