package cisc889

import java.io.File

import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document.Document
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.store.FSDirectory
import org.apache.lucene.util.Version

case class Indexer()

object Indexer {

	def apply(src: File, dst: File): Indexer = {
		val analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT)
		val conf = new IndexWriterConfig(Version.LUCENE_CURRENT, analyzer)

		conf.setOpenMode(IndexWriterConfig.OpenMode.CREATE)

		val dir = FSDirectory open dst

		val index = new IndexWriter(dir, conf)

		val dats = src listFiles () filter (_.getName endsWith ".dat")

//		val iters = dats.view map { new TrecDocIterator(_).getDocuments.iterator }

		dats foreach { dat =>
			{
				val iter = new TrecDocIterator(dat).getDocuments.iterator
				while (iter.hasNext) {
					val docu = iter.next().asInstanceOf[Document]
					index.addDocument(docu)
				}
			}
		}

		index.close()

		new Indexer()
	}
}
