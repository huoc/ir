package cisc889

case class HtmlParser(val queries : Iterable[Query])

object HtmlParser {
	def apply(path : String) : HtmlParser = {
		val topics = scala.io.Source.fromFile(path).mkString
		val topmat = """<top>"""
		
		val tops = topics.split(topmat)

		val toFind = """<num> Number: (\d+).*\n<title> (.*)""".r
		val queries = tops.toList flatMap {topic => for {
			toFind(id, query) <- toFind findAllIn topic
		} yield new Query(id, query)}

		new HtmlParser(queries)
	}
}