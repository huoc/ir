package cisc889

import java.io.File
import java.io.FileWriter

object Project1 {

	val home = System.getenv("HOME")
	val proj = "/Courses/ir/proj1"
	val topics = home+proj+"/collections/topics.trec8.401-450"
	val topFile = home+proj+"/input.proj1.top"

	def main(args : Array[String]) = {
		val indexFile = new File(home, proj + "/index")
//		val indexer = Indexer(new File(home, proj + "/collections"), indexFile)
		
		val queries = HtmlParser(topics).queries
		val searcher = Searcher(indexFile, queries)

		val output = new FileWriter(topFile)
		output.write(searcher.formatted)
		output.close
	}
}