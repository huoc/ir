package cisc889

case class Query(val id : String, val q : String) {
	override def toString = id + ": " + q
}

object Query {
	def apply(raw : String): Query = new Query((raw split " ")(0), (raw split " ")(1))
}

object Queries {
	def apply(f : String): Iterable[Query] = {
		val lines = scala.io.Source.fromFile(f).getLines.toList
		lines map {Query(_)}
	}
	
	def apply(id : String, q : String) = new Query(id, q)
}