package cisc889;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

public class TrecDocIterator
{

    public TrecDocIterator(File file)
        throws IOException
    {
        docs = new ArrayList<Field>();
        parse(file);
    }

    public void parse(File file)
        throws IOException
    {
        BufferedReader bufferedreader = new BufferedReader(new FileReader(file));
        StringBuffer stringbuffer = new StringBuffer(100);
        do
        {
            if(!bufferedreader.ready())
                break;
            String s = bufferedreader.readLine();
            if(s.indexOf("<DOCNO>") != -1)
            {
                if(s.indexOf("</DOCNO>") == -1)
                    throw new RuntimeException("DOCNO Mallformed");
                doc = createDoc(s);
                docs.add(doc);
            } else
            if(doc != null)
            {
                String s1 = stripTagsOut(s);
                stringbuffer.append((new StringBuilder()).append(" ").append(s1).append(" ").toString());
            }
            if(s.indexOf("</DOC>") != -1)
            {
                doc.add(new Field("text", stringbuffer.toString(), org.apache.lucene.document.Field.Store.NO, org.apache.lucene.document.Field.Index.ANALYZED, org.apache.lucene.document.Field.TermVector.YES));
                stringbuffer = new StringBuffer(100);
            }
        } while(true);
        bufferedreader.close();
    }

    public String stripTagsOut(String s)
    {
        boolean flag = false;
        StringBuffer stringbuffer = new StringBuffer();
        for(int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if(c == '<')
            {
                flag = true;
                continue;
            }
            if(c == '>')
            {
                flag = false;
                continue;
            }
            if(!flag)
                stringbuffer.append(c);
        }

        String s1 = stringbuffer.toString();
        s1 = s1.replaceAll("&blank;|&hyph;|&amp;|&sect;|&bull;", " ");
        return s1;
    }

    public Document createDoc(String s)
    {
        StringTokenizer stringtokenizer = new StringTokenizer(s, " \t\n\r\f<>");
        stringtokenizer.nextToken();
        String s1 = stringtokenizer.nextToken().trim();
        Document document = new Document();
        document.add(new Field("DOCNO", s1, org.apache.lucene.document.Field.Store.YES, org.apache.lucene.document.Field.Index.NOT_ANALYZED));
        return document;
    }

    public ArrayList<Field> getDocuments()
    {
        return docs;
    }

    private Document doc;
    private ArrayList docs;
}
