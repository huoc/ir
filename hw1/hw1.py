from __future__ import division
import sys, math, numpy

class DupDict(dict):
	def __setitem__(self, key, value):
		try:
			self[key]
		except KeyError:
			super(DupDict, self).__setitem__(key, [])
		self[key].append(value)

class Qrels:
	def __init__(self, name):
		try: 
			f = open(name)
		except IOError as e:
			print e
			raise
		
		def pair(l):
			lst = l.split()
			return {(lst[0],lst[2]) : int(lst[3]) if int(lst[3]) > -1 else 0}

		p = pair

		raw = f.readlines()
		f.close()
		#query, doc : rele
		self.data = reduce(lambda x, y: dict(x.items() + pair(y).items()), raw, dict())
		#query : doc, rele
		self.dataOnQ = DupDict()
		[self.dataOnQ.__setitem__(x.split()[0], \
			(x.split()[2], int(x.split()[3]) if int(x.split()[3]) > -1 else 0)) for x in raw]

	def sizeForQuery(self, query):
		return len(self.dataOnQ[query])

	def relevance(self, q, doc):
		'''query and doc are strings and returns an int'''
		try:
			return self.data[(q, doc)]
		except KeyError:
			return 0

	def totalRelevent(self, q):
		return len(filter(lambda x: x[1] > 0, self.dataOnQ[q]))

	def query2Rele(self, query):
		'''given a query, pull out a sorted list of rele values'''
		return reversed(sorted([x[1] for x in self.dataOnQ[query]]))

class Tops:
	def __init__(self, name):
		try:
			f = open(name)
		except IOError as e:
			print e
			raise

		self.data = DupDict()
		[self.data.__setitem__(x.split()[0], (x.split()[2], int(x.split()[3]))) \
													for x in f.readlines()]

		print self.data
		f.close()

	def getByQuery(self, query):
		'''query : doc, rank'''
		d = self.data[query]
		return sorted(d, key=lambda x: x[1])

	def sizeForQuery(self, query):
		return len(self.data[query])

class Solver:
	def __init__(self, qrels, top):
		self.qr = qrels
		self.top = top

	def isRele(self, q, d):
		return self.qr.relevance(q, d) > 0

	def relevantN(self, query, n):
		'''get back the number of relevant for 1st n'''
		lst = self.top.getByQuery(query)
		return len(filter(lambda x: self.isRele(query, x[0]), lst[:n]))

	def precisionN(self, query, n):
		return self.relevantN(query, n)/n

	def recallN(self, query, n):
		return  self.relevantN(query, n)/self.qr.totalRelevent(query)

	def FN(self, query, n):
		p = self.precisionN(query, n)
		r = self.recallN(query, n)
		if p * r == 0.0:
			return 0.0
		else:
			return 2 / (1 / self.precisionN(query, n) + 1 / self.recallN(query, n))

	def avgPrecision(self, query):
		size = self.top.sizeForQuery(query)

		def _isRele(index):
			return self.isRele(query, self.top.getByQuery(query)[index][0])

		return reduce(lambda avg, n: avg + self.precisionN(query, n+1) \
					if _isRele(n) else avg, \
					range(size), 0.0)/self.qr.totalRelevent(query)

	def DCGN(self, query):
		#(doc, rank)
		#dr = self.top.getByQuery(query)[:n]
		dr = self.top.getByQuery(query)

		def one(query, doc, rank):
			return (2 ** self.qr.relevance(query, doc)-1) / math.log(rank+1, 2)

		return reduce(lambda x, y: x + one(query, y[0], y[1]), dr, 0)

	def iDCGN(self, query):
		'''something with qr only'''	
		dr = self.qr.query2Rele(query)
		#size = self.top.sizeForQuery(query)[:n]
		size = self.top.sizeForQuery(query)

		def one(rele, rank):
			return (2 ** rele - 1) / math.log(rank+1, 2)

		return reduce(lambda x, y: x + apply(one, y), zip(dr, range(1, size+1)), 0)

	def nDCGN(self, query):
		return self.DCGN(query) / self.iDCGN(query)

			
		
def res(s, query, fmt):
	'''returns a string'''
	return fmt % (query, s.precisionN(query, 10), s.recallN(query, 10), \
				s.FN(query, 10), s.avgPrecision(query), s.DCGN(query), s.nDCGN(query))

def res2(s, query, fmt):
	'''returns a string'''
	return fmt % (query, s.avgPrecision(query), s.nDCGN(query))

def ttest(queries, l1, l2):
	mean1 = numpy.mean(l1)
	mean2 = numpy.mean(l2)
	mean_diff = mean1 - mean2
	var = numpy.var([x[0]-x[1] for x in zip(l1, l2)])
	return mean_diff / math.sqrt(var/len(queries))

if __name__ == '__main__':
	queries = [('Q-02%d' % d) for d in range(8)]

	header = ', precision@10, recall@10, F@10, average precision, DCG, nDCG\n'
	fmt = '%s, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f\n'

	q1 = Qrels('qrels1')
	q2 = Qrels('qrels2')

	bing = Tops('BM25.top')
	google = Tops('LM.top')

	s_q1_bing = Solver(q1, bing)
	s_q1_google = Solver(q1, google)
	
	res_bing = reduce(lambda x, q: x + res(s_q1_bing, q, fmt), queries, 'bing'+header)
	res_google = reduce(lambda x, q: x + res(s_q1_google, q, fmt), queries, 'google'+header)
	print res_google
	print res_bing
	p1 = [s_q1_bing.avgPrecision(q) for q in queries]
	p2 = [s_q1_google.avgPrecision(q) for q in queries]
	t1 = ttest(queries, p1, p2)
	print t1
	
	s_q2_bing = Solver(q2, bing)
	s_q2_google = Solver(q2, google)

	header2 = ', avg precision, nDCG\n'
	fmt2 = '%s, %.6f, %.6f\n'

	res_bing2 = reduce(lambda x, q: x + res2(s_q2_bing, q, fmt2), queries, 'bing'+header2)
	res_google2 = reduce(lambda x, q: x + res2(s_q2_google, q, fmt2), queries, 'google'+header2)
	print res_google2
	print res_bing2
	d1 = [s_q2_bing.nDCGN(q) for q in queries]
	d2 = [s_q2_google.nDCGN(q) for q in queries]
	t2 = ttest(queries, d1, d2)
	print t2

	f = open('result.csv', 'w')
	f.write('\n'.join([res_bing, res_google, 't-test: '+ str(t1), \
				res_bing2, res_google2, 't-test: ' + str(t2)]))
	f.close()
