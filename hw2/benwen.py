from operator import itemgetter

def writeAns(docs, fileName, metric):
	Q=['Q-020','Q-021','Q-022','Q-023','Q-024','Q-025','Q-026','Q-027']
	file = open(fileName, "w")
	for q in Q:
		res=[]
		for d in docs:
			if metric=="BM25":
				res.append((d.name,d.bm_rating[q]))
			else:
				res.append((d.name,d.lm_rating[q]))
		res.sort(key=itemgetter(1),reverse=True)
		rank=1
		for r in res[:22]:
			file.write(q)
			file.write(" XX ")
			file.write(r[0].split('.')[0])
			file.write(" ")
			file.write('{}'.format(rank))
			file.write(" ")
			file.write('{}'.format(r[1]))
			file.write(" ")
			file.write(metric)
			file.write("\n")
			rank=rank+1
	file.close()
	
