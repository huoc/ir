from __future__ import division
import math

'''Constant'''
k1=2
b=0.75
mu=2000

class RatingSystem:
	def __init__(self, corpus):
		'''one per corpus'''
		self.documents = corpus

		self.collection = self.doc2col(corpus)
		self.avgLength = self.avgLen(corpus)
		self.N = len(corpus)
		self.lenC = len(self.collection)
		self.numberOfDocuments = len(self.documents)
		
		self.ctf_cache = {}
		self.df_cache = {}

	def getDocs(self):
		return self.documents

	def doc2col(self, corpus):
		return reduce(lambda x, y: x + y, corpus, [])

	def avgLen(self, corpus):
		return sum(map(len, corpus))/len(corpus)

	def df(self, q):
		if q in self.df_cache:
			return self.df_cache[q]
		else:
			df = reduce(lambda x, y: x+y.count(q), self.documents, 0)
			self.df_cache[q] = df
			return df

	def tf(self, q, D):
		return D.count(q)

	def tfc(self, q):
		return self.collection.count(q)

	def bm25(self, Q, D):
		'''Q: a whole query, D: a doc'''

		def aQ(q):
			tfqD = self.tf(q, D)
			dfq = self.df(q)
			return tfqD * (k1 + 1) / \
					(tfqD + k1 * (1-b+b*len(D)/self.avgLength)) *\
					math.log((self.N-dfq+0.5)/(dfq+0.5))

		return reduce(lambda x, q: x + aQ(q), Q, 0.0)

	def lm(self, Q, D):
		
		def aQ(q):
			tfqD = self.tf(q, D)
			tfqQ = self.tf(q, Q)
			ctfq = self.tfc(q)

			#!!!!!!!
			if ctfq == 0: ctfq = 1.0

			return tfqQ * math.log((tfqD+mu*(ctfq/self.lenC))/(len(D) + mu))

		return reduce(lambda x, q: x + aQ(q), Q, 0.0)
