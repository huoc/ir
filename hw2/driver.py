from formula import RatingSystem
from document import *
from itertools import product
import json
from benwen import writeAns

FOLDER = 'WEB-889-14'

class MyEncoder(json.JSONEncoder):
	def default(self, o):
		return o.__dict__

class sQ():
	def __init__(self, raw):
		self.raw = raw
		
	def getID(self):
		'''id'''
		return self.raw.split()[0]

	def getQuery(self):
		'''stemmed_query'''
		return map(myStem, self.raw.split()[1:])

def cache(docs, fname):
	with open(fname, 'a') as f:
		json.dump(docs, f, cls = MyEncoder, encoding='utf-8')

def fetch(fname):

	def from_json(ob):
		if 'name' in ob: 
			bm = ob['bm_rating']
			if bm == None: bm = {}
			lm = ob["lm_rating"]
			if lm == None: lm = {}
			return Document(ob['name'], ob['title'], bm, lm)

	f = open(fname)
	obs = json.JSONDecoder(object_hook=from_json).decode(f.read())
	f.close()
	return obs

def main():
	if os.path.isfile('cache.json'):
		docs = fetch('cache.json')
	else:
		edocs = map(lambda f: Document(f), os.listdir(FOLDER))
		docs = filter(lambda x: x.title != '', edocs)
		cache(docs, 'cache.json')

	r = RatingSystem(getCorpus(docs))

	qfile = open('queries')
	rawQueries = qfile.readlines()
	qfile.close()

	Qs = map(lambda x: sQ(x), rawQueries)

	ts = product(docs, Qs)

	for (d, Q) in ts:
		d.updateBMRating(Q, r)
		d.updateLMRating(Q, r)

	cache(docs, 'results.json')

	return docs


if __name__ == '__main__':
	docs = main()

	writeAns(docs, 'BM25.top','BM25')
	writeAns(docs, 'LM.top','LM')
	#for d in docs: print d
