import os
from bs4 import BeautifulSoup
from nltk.stem.lancaster import LancasterStemmer as Stemmer
from driver import FOLDER

MEANINGLESS = ['-', 'amp', '|', '', 'quote', '&']
TITLE_SEP = ' '

def getCorpus(docs):
	return map(lambda x: x.getStemmedTitle(), docs)

def myStem(string):
	return Stemmer().stem(string.lower().strip('\t,\r\n()'))


class Document():
	'''a doc has name, rating based on some query'''

	def __init__(self, name, title=None, bm_rating={}, lm_rating={}):
		'''title is stemmed'''
		self.name = name
		if title == None:
			self.title = self.getStemmedTitle()
		else:
			self.title = title

		self.bm_rating = bm_rating
		self.lm_rating = lm_rating
		print self.__dict__


	def getStemmedTitle(self):

		def readContent(path):
			temp = open(path)
			data = temp.read()
			temp.close()
			return data

		d = readContent(os.path.join(FOLDER, self.name))
		bs = BeautifulSoup(d)

		if bs.title == None: return ''
		else: 
			return filter(lambda x: x not in MEANINGLESS, \
							map(myStem, bs.title.string.split(TITLE_SEP)))

	def updateBMRating(self, Q, r):
		print Q.getID()
		print Q.getQuery()
		print self
		self.bm_rating[Q.getID()] = r.bm25(Q.getQuery(), self.title)

	def updateLMRating(self, Q, r):
		self.lm_rating[Q.getID()] = r.lm(Q.getQuery(), self.title)

	def __str__(self):
		return self.name + '\n' + str(self.title) + '\n' + '\n'.join(map(str, self.bm_rating.items())) \
					+ '\n' + '\n'.join(map(str, self.lm_rating.items()))
